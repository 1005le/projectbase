package com.rikkei.projectbase.injection;

import com.rikkei.projectbase.utils.rx.AppRxSchedulers;
import com.rikkei.projectbase.utils.rx.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {
    @Provides
    RxSchedulers provideRxSchedulers() {
        return new AppRxSchedulers();
    }
}
