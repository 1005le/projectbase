package com.rikkei.projectbase.injection;

import android.support.annotation.NonNull;

import com.rikkei.projectbase.interactor.MainInteractor;
import com.rikkei.projectbase.interactor.impl.MainInteractorImpl;
import com.rikkei.projectbase.presenter.loader.PresenterFactory;
import com.rikkei.projectbase.presenter.MainPresenter;
import com.rikkei.projectbase.presenter.impl.MainPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public final class MainViewModule {
    @Provides
    public MainInteractor provideInteractor() {
        return new MainInteractorImpl();
    }

    @Provides
    public PresenterFactory<MainPresenter> providePresenterFactory(@NonNull final MainInteractor interactor) {
        return new PresenterFactory<MainPresenter>() {
            @NonNull
            @Override
            public MainPresenter create() {
                return new MainPresenterImpl(interactor);
            }
        };
    }

}
