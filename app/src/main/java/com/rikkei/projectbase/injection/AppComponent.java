package com.rikkei.projectbase.injection;

import android.content.Context;

import com.rikkei.projectbase.App;
import com.rikkei.projectbase.network.request.Apis;
import com.rikkei.projectbase.utils.rx.RxSchedulers;
import com.rikkei.projectbase.utils.sharedpreference.RxPreferenceHelper;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class, RxModule.class, SharedPreferenceModule.class})
public interface AppComponent {
    Context getAppContext();

    App getApp();

    Apis appApis();

    RxSchedulers rxSchedulers();

    RxPreferenceHelper getPreference();
}