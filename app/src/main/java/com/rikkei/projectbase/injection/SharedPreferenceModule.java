package com.rikkei.projectbase.injection;

import android.content.Context;

import com.rikkei.projectbase.utils.sharedpreference.PreferencesHelper;
import com.rikkei.projectbase.utils.sharedpreference.RxPreferenceHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SharedPreferenceModule {
    @Provides
    @Singleton
    public RxPreferenceHelper providePreferencesHelper(Context context) {
        return PreferencesHelper.getInstance(context);
    }
}
