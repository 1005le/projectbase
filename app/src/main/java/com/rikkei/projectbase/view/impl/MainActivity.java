package com.rikkei.projectbase.view.impl;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.rikkei.projectbase.R;
import com.rikkei.projectbase.injection.DaggerMainViewComponent;
import com.rikkei.projectbase.view.MainView;
import com.rikkei.projectbase.presenter.loader.PresenterFactory;
import com.rikkei.projectbase.presenter.MainPresenter;
import com.rikkei.projectbase.injection.AppComponent;
import com.rikkei.projectbase.injection.MainViewModule;

import javax.inject.Inject;

public final class MainActivity extends BaseActivity<MainPresenter, MainView> implements MainView {
    @Inject
    PresenterFactory<MainPresenter> mPresenterFactory;

    // Your presenter is available using the mPresenter variable

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Your code here
        // Do not call mPresenter from here, it will be null! Wait for onStart or onPostCreate.
    }

    @Override
    protected void setupComponent(@NonNull AppComponent parentComponent) {
        DaggerMainViewComponent.builder()
                .appComponent(parentComponent)
                .mainViewModule(new MainViewModule())
                .build()
                .inject(this);
    }

    @NonNull
    @Override
    protected PresenterFactory<MainPresenter> getPresenterFactory() {
        return mPresenterFactory;
    }
}
