package com.rikkei.projectbase.presenter;

import com.rikkei.projectbase.view.MainView;

public interface MainPresenter extends BasePresenter<MainView> {

}